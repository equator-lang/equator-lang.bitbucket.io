function listFromJSON(src, callback) {
    // Disabled incorrect error messages in some browsers.
    $.ajaxSetup({'beforeSend': function(xhr) {
            if (xhr.overrideMimeType) {
                xhr.overrideMimeType("text/plain");
            }
        }
    });

    $.getJSON(src, {}, callback);
}

function updateListByJSON(elementId, src) {
    var list = document.getElementById(elementId);

    listFromJSON(src, function (responce) {
        // Wipe it.
        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }

        for (var i = 0; i < responce.length; i++) {
            var elem = document.createElement("li");

            var link = document.createElement("a");
            link.setAttribute("href", responce[i].path);
        
            var linkText;
            if (responce[i].path == "#") {
                link.setAttribute("onclick", "return false");
                linkText = document.createTextNode(responce[i].name + " (coming soon)");
            } else {
                linkText = document.createTextNode(responce[i].name);
            }

            link.appendChild(linkText);
            elem.appendChild(link);
            list.appendChild(elem);
        }
    });
}

function updateSectionByJSON(elementId, src) {
    var list = document.getElementById(elementId);

    listFromJSON(src, function (responce) {
        // Wipe it.
        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }

        for (var i = 0; i < responce.length; i++) {
            var container = document.createElement("div");
            container.className = "dynamicSectionContainer";

            var title = document.createElement("div");
            title.className = "dynamicSectionTitle";
            title.innerHTML = responce[i].title;

            var description = document.createElement("div");
            description.className = "dynamicSectionDescription";
            description.innerHTML = responce[i].description;

            container.appendChild(title);
            container.appendChild(description);
            list.appendChild(container);
        }
    });
}

function collapseDrop(dropContent, dropButton) {
    var content = document.getElementById(dropContent);
    var button = document.getElementById(dropButton);

    if (content.className == "dropContentExpanded dropContent") {
        content.className = "dropContentCollapsed dropContent";
    }

    if (button.className == "dropBoxSelected dropBox") {
        button.className = "dropBoxUnselected dropBox";
    }
}

function expandDrop(dropContent, dropButton) {
    var content = document.getElementById(dropContent);
    var button = document.getElementById(dropButton);

    if (content.className == "dropContentCollapsed dropContent") {
        content.className = "dropContentExpanded dropContent";
    }

    if (button.className == "dropBoxUnselected dropBox") {
        button.className = "dropBoxSelected dropBox";
    }
}

function openLeftDrop() {
    expandDrop("leftDropContent", "leftDrop");
    collapseDrop("midDropContent", "midDrop");
    collapseDrop("rightDropContent", "rightDrop");
}

function openMidDrop() {
    collapseDrop("leftDropContent", "leftDrop");
    expandDrop("midDropContent", "midDrop");
    collapseDrop("rightDropContent", "rightDrop");
}

function openRightDrop() {
    collapseDrop("leftDropContent", "leftDrop");
    collapseDrop("midDropContent", "midDrop");
    expandDrop("rightDropContent", "rightDrop");
}

function smoothScroll(selection) {
    document.querySelector(selection).scrollIntoView({
        behavior: 'smooth'
    });
}